//setup the dependencies
const express = require("express");
const router = express.Router();
//import the controller file
const courseController = require("../controllers/course");
const auth = require("../auth");

//route to get all courses
router.get("/", (req, res) => {
	courseController.getCourses().then(resultFromController => res.send(resultFromController))
})

//route to get specific course

router.get("/:courseId", (req, res) => {
	//"/:courseId" here is called a "wildcard" and its value is anything that is added at the end of localhost:4000/courses

	//e.g. localhost:4000/courses/dog = the value of our wildcard is "dog"

	// console.log(req.params)

	//req.params always gives an object. The key for this object is our wildcard, and the value comes from the request endpoint

	courseController.getSpecific(req.params.courseId).then(resultFromController => res.send(resultFromController))
})

//route to create a new course

//when a user sends a specific method to a specific endpoint (in this case a POST request to our /courses endpoint) the code within this route will be run
router.post("/", auth.verify, (req, res) => {
	//auth.verify here is something called "middleware." Middleware is any function that must first be resolved before any succeeding code will be executed.

	//if your middleware does not continue, then the rest of your code will NOT be executed.

	//show in the console the request body
	// console.log(req.body)

	//invoke the addCourse method contained in the courseController module, which is an object. Pages, when imported via Node are treated as objects.

	//We also pass req.body to addCourse as part of the data that it needs

	//Once addCourse has resolved, .then can send whatever it returns (true or false) as its response

	// console.log(auth.decode(req.headers.authorization))

	/*
	MINI ACTIVITY (10 Minutes):
	Change the code below to restrict course creation to ONLY admins.

	If a user is not an admin, send a response of false
	*/

	if(auth.decode(req.headers.authorization).isAdmin){
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}

})

//route to update a single course
router.put("/:courseId", auth.verify, (req, res)=>{
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}
)

//route to archive a single course
router.delete("/:courseId", auth.verify, (req, res)=>{
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}
)

//export the router
module.exports = router;
